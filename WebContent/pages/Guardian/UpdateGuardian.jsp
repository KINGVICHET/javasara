<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="bean.ProvinceBean" %>
<%@ page import="bean.CommuneBean" %>
<%@ page import="bean.CountryBean" %>
<%@ page import="bean.DistrictBean" %>
<%@ page import="bean.VillageBean" %>
<%@ page import="db.services.ProvinceService" %>
<%@ page import="db.services.TeacherService" %>
<%@ page import="db.services.DistrictService" %> 
<%@ page import="db.services.VillageService" %> 
<%@ page import="db.services.CommuneService" %>
<%@ page import="bean.GuardianBean" %>
<%@ page import= "db.services.GuardianService" %>
<%
	session = request.getSession(false);
	String role_code = "",role_name="",user_name="";
	String photo_view = "";
	if(session != null){
		
		if(session.getAttribute("user_name")!= null){
			user_name = session.getAttribute("user_name").toString();
			photo_view = "." + session.getAttribute("photo_url").toString() + "/" + session.getAttribute("photo_name").toString();
					
			role_code = session.getAttribute("role_code").toString();
			role_name = session.getAttribute("role_name").toString();
			
		}else{
			response.sendRedirect("../../AccessSystem");
		}
	}else{
		//redirect to login page
		response.sendRedirect("../../AccessSystem");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>LMS</title>

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/font-awesome.min.css" rel="stylesheet">
    <link href="../../webcss/all.min.css" rel="stylesheet"> 
    <link href="../../css/custom.min.css" rel="stylesheet">
     <link href="../../css/customStyle/MaltyFormData.css" rel="stylesheet">
     <link href="../../css/sweatalert_css/sweetalert2.min.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script> 
    <script src="../../js/custom_js/lock_screen.js"></script>
    <script src="../../js/custom_js/sweatAlert2_js/sweetalert2.min.js"></script>
    
    <script>
           
   
		
	
	$(document).ready(function(){
		$("#current_province").on('change', function()
		{
			var params = {proid : $(this).val()};
				$.post("${pageContext.request.contextPath}/ProvinceServlet" , $.param(params) , function(responseJson)
						{
							var $select = $("#current_district");
							$select.find("option").remove();
							$.each(responseJson, function(idx, key)
							{
								console.log( key );
								$("<option>").val( key.district_id ).text(key.district).appendTo($select);
					        });
				        });
		});
	});
	
	

	$(document).ready(function(){
		$("#current_district").on('change', function()
		{
			var params = {distid : $(this).val()};
				$.post("${pageContext.request.contextPath}/SelectCommuneServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#current_commune");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.commune_id ).text(key.commune).appendTo($select);
					});
				});
		});
	});
		
	

	$(document).ready(function(){
		$("#current_commune").on('change', function()
		{
			
			var params = {comid : $(this).val()};				
				$.post("${pageContext.request.contextPath}/SelectVillageServlet" , $.param(params) , function(responseJson)
				{
					var $select = $("#current_village");
					$select.find("option").remove();
					$.each(responseJson, function(idx, key)
					{
						console.log( key );
						$("<option>").val( key.village_id ).text(key.village).appendTo($select);
					});
				});
		});
	});
	</script>
  	
	
    		
  </head>
<body class="nav-md">

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="#" class="site_title"><i class="fas fa-school fa-2x"></i> <span>School<b>MS</b></span></a>
            </div>

            <!-- sidebar menu -->
			<jsp:include page="/view/detail/LeftMenu">
				<jsp:param value="<%=user_name %>" name="user_name"/>
				<jsp:param value="<%=photo_view %>" name="photo_view"/>
				<jsp:param value="<%=role_name %>" name="role_name"/>
				<jsp:param value="<%=role_code %>" name="role_code"/>
			</jsp:include>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
       	<jsp:include page="/view/detail/Banner">
       		<jsp:param value="<%=user_name %>" name="user_name"/>
       		<jsp:param value="<%=photo_view %>" name="photo_view"/>
       		<jsp:param value="<%=role_name %>" name="role_name"/>
       	</jsp:include>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2 id="title_student">		Update Guardian Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
						
                  </div>
                  <form role="form" class="registration-form" action="${pageContext.request.contextPath}" Method="POST"  enctype="multipart/form-data" >
                    <%
                  		String guardian_id = request.getParameter("id");
             			GuardianBean gd = GuardianService.p_getAGuardian(guardian_id);
             			
             			
                  	 %>
             
              <div class="row">
                                
                                
                  <div class="col-md-9 col-sm-12" id="bordy_top" >
	                      <div class="form-group row" >
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Teacher ID :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                             <input type="text" value="<%=gd.getGuardian_id() %>" class="form-control" disabled id="teacher_id" >
	                               <input type="hidden" id="guardian_id"  name="guardian_id" value="<%=gd.getGuardian_id() %>" class="form-control" autocomplete="off">
	                           </div>
	                      </div>
	                      
	                      <div class="form-group row" id="bordy_first_name">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">First Name :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                           <small class="text-danger" id="fname_error"></small>
	                           <input type="text" class="form-control" value="<%=gd.getFather_name() %>"  id = "guardian_name"  name = "guardian_name" autocomplete="off">
	                         </div>
	                      </div>
	                      
	                     <div class="form-group row" id="bordy_last_name" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Father Job :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger bold italic" id="lname_error"></small>
	                             <input type="text" class="form-control" id= "father_job" value="<%=gd.getFather_job() %>" name="father_job" autocomplete="off">
	                        </div>
	                     </div>
	                    
	                      
	                 <div class="form-group row "  id="bordy_dob">
	                    <label class="control-label col-md-4 col-sm-3 col-xs-3 ">Mother Job :</label>
	                        <div class="col-md-8 col-sm-9 col-xs-9 input-error" >
	                            <small class="text-danger bold italic" id="dob_error"></small>
	                                <input type="text" class="form-control" name="mother_job" id="mother_job" autocomplete="off" value="<%= gd.getMother_name() %>">
	                           </div>
	                        </div>
                       </div>
                   </div>
                   <div class="form-group row" id="bordy_last_name" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Mother Job :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger bold italic" id="lname_error"></small>
	                             <input type="text" class="form-control" id= "mother_job" value="<%=gd.getMother_job() %>" name="mother_job" autocomplete="off">
	                        </div>
	               </div>	
	               	<div class="form-group row" id="bordy_last_name" >
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Phone Number :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                            <small class="text-danger bold italic" id="lname_error"></small>
	                             <input type="text" class="form-control" id= "phone_number" value="<%=gd.getGuardian_phone() %>" name="phone_number" autocomplete="off">
	                        </div>
	               </div>		
       
                    
                     
	                    <div class="form-group row"  id="bordy_dob">
	                         <label class="control-label col-md-4 col-sm-3 col-xs-3">Province :</label>
	                             <div class="col-md-8 col-sm-9 col-xs-9">
	                                <small class="text-danger" id="current_province_error"></small>
	                                   <select class="form-control"​​​ id="current_province" name="current_province" >
	                             	     <option   selected disabled value="0" >Choose Current Province</option>
	                                	       <% 
                                	             		ArrayList<ProvinceBean> provinceBean = ProvinceService.p_listAllProvince();
                                	             		
		                      		                   for(ProvinceBean cp : provinceBean)
					                      		         {
			                      		            	   if(cp.getPro_id().equals(gd.getAb().getProvince_id())){
					                      			          out.print("<option value='"+ cp.getPro_id()+"' selected>" + cp.getProvince()+ "</option>");
					                      		        } else{
					                      		        	   out.print("<option value='"+ cp.getPro_id()+"'>" + cp.getProvince()+ "</option>");
					                      		           }
			                      		            	}
		                      		             %>
                            	   </select>
	                          </div>
	                      </div>
                               
                              
                               
                               
                               
                               
                               
                               
                        <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">District :</label>
	                            <div class="col-md-8 col-sm-9 col-xs-9">
	                               <small class="text-danger" id="current_district_error"></small>
	                                  <select class="form-control"​​​ id="current_district" name="current_district" >
	                             	<option  id ="current-district-index" class="hidden"  selected disabled value= "0">Choose Current District</option>
	                             	       <% 
	                             	       
		                             		String  curr_province_id = gd.getAb().getProvince_id();
		                           			String curr_district_id = gd.getAb().getDistrict_id();
		                             	   ArrayList<DistrictBean> districtBean = DistrictService.p_listAllDistrictByProvinceID(curr_province_id);
                 		                    for(DistrictBean AD : districtBean ) 
                 		                    {
                     		            	   if( AD.getDistrict_id().equals(curr_district_id))
                     		            	   {
                     		            		    out.print("<option id ='district-index' class='hidden'  selected disabled value ='0' >ជ្រើសរើស ស្រុក</option>"); 
		                      			      		out.print("<option value='"+ AD.getDistrict_id() +"' selected >" + AD.getDistrict()+ "</option>");
		                      		           } 
                     		            	   else
                     		            	   {
		                      		        	 		out.print("<option value='"+ AD.getDistrict_id() +"'>" + AD.getDistrict()+ "</option>");
		                      		           }
                     		            	}
					                     %>    
                            	</select>
	                          </div>
	                     </div>
	                     
	                       
                       <div class="form-group row"  id="bordy_dob">
	                       <label class="control-label col-md-4 col-sm-3 col-xs-3">Commune:</label>
	                          <div class="col-md-8 col-sm-9 col-xs-9">
	                              <small class="text-danger" id="current_commune_error"></small>
	                                 <select class="form-control"​​​ id="current_commune" name="current_commune" >
	                              <option  selected disabled value="0" >Choose Current Commune</option>
	                                        <%
			                             		
				                             	   ArrayList<CommuneBean>  commune = CommuneService.p_listAllCommuneByDistrictID(gd.getAb().getDistrict_id());
	                      		                    for( CommuneBean cc : commune  )
				                      		         {
		                      		            	   if( cc.getCommune_id().equals(gd.getAb().getCommune_id())){
				                      			      out.print("<option value='"+ cc.getCommune_id() +"' selected >" + cc.getCommune()+ "</option>");
				                      		        } else{
				                      		        	 out.print("<option value='"+ cc.getCommune_id()+"'>" + cc.getCommune()+ "</option>");
				                      		           }
		                      		            	}
					                             
					                      		%>
                               </select>
	                        </div>
	                     </div>
	                     
                         <div class="form-group row"  id="bordy_dob">
	                        <label class="control-label col-md-4 col-sm-3 col-xs-3">Village :</label>
	                           <div class="col-md-8 col-sm-9 col-xs-9">
	                             <small class="text-danger" id="current_village_error"></small>
	                                <select class="form-control"​​​ id="current_village" name="current_village" >
	                             <option id="current-village-index" class="hidden"  selected disabled value="0" >Choose Current Village</option>
	                              <%
				                             	   ArrayList<VillageBean> villageBean = VillageService.p_listAllVillageByCommuneID(gd.getAb().getCommune_id());
	                      		                    for( VillageBean VB : villageBean  )
				                      		         {
		                      		            	   if( VB.getVillage_id().equals(gd.getAb().getVillage_id())){
				                      			      out.print("<option value='"+ VB.getVillage_id() +"' selected >" + VB.getVillage()+ "</option>");
				                      		        } else{
				                      		        	 out.print("<option value='"+ VB.getVillage_id() +"'>" + VB.getVillage()+ "</option>");
				                      		           }
		                      		            	}
					                             
					                      		%>
                            	</select>
	                        </div>
	                     </div>
								<center>
								
									<button type="button" class="btn btn-previous btn-danger" id='previous-2'>Back</button>
                                    <button type="submit" class="btn btn-next btn-primary"   >Update</button>
								</center>
                            
                       
                    
                    
        
                    
                </form>
            </div>
        </div>
    </div>      
           </div>
      
		<jsp:include page="/view/detail/FooterPage"></jsp:include>   
      </div>
	
    <script src="../../js/bootstrap.bundle.min.js"></script>
    <script src="../../js/custom.min.js"></script>
	
</body>
</html>