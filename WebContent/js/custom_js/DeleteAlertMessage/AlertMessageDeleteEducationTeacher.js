
    $(document).ready(function(){
        
        $(document).on('click','.btnEdudelet',function(e){
            
            var Id = $(this).val();
            ShowMessage(Id);
            e.preventDefault();
        });
        
    });
    
    function ShowMessage(Id){
       
        swal({
           title: 'Are You Sure?',
           text: "You Are Deleted Row This..." + Id,
           type: 'warning',
           showCancelButton: true,
           confirmButtonColor: '#3085d6',                
           cancelButtonColor: '#d33',
           cancelButtonText: 'No',
           confirmButtonText: 'Yes',
           showLoaderOnConfirm: true,
           
preConfirm: function() {
 return new Promise(function(resolve){   
                   $.ajax({
                       url:'../../DeleteTeacherEductionById',              
                       type:'POST',
                       data:'id='+Id,
                       dataType:'JSON'
                   })
                   .done(function(response){
                        swal('Deleted Row Success!', response.message, response.status); 
                        location.reload();
                   })
                   .fail(function(){
                       swal('Oops...', 'Error with ajax !', 'error');    
                   });
               });
           },
           allowOutsideClick: false
        });
        
    }
   

	
