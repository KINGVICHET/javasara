package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import bean.Sub_SubjectBean;
import bean.SubjectBean;
import bean.SubjectCategoryBean;
import db.mysql.MySQL;

public class Sub_SubjectService {
	
	public static SubjectBean getAsubjectCategory(String id){
		
		SubjectBean sb = new SubjectBean();
		try{
			String sql = "SELECT t_subject_category.*,t_subject.*,t_sub_subject.* "
					   + "FROM t_subject_category INNER JOIN (t_subject INNER JOIN "
					   + "t_sub_subject ON t_subject.subject_id = t_sub_subject.subject_id )"
					   + " ON t_subject_category.category_id = t_subject.category_id "
					   + "WHERE t_subject.subject_id = ?";
			
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
			 
			  sb.setSubject_id(rs.getString("subject_id"));	
			  sb.setSubject_name(rs.getString("subject_name"));
			  SubjectCategoryBean catbean = new SubjectCategoryBean();
			  catbean.setCategory_id(rs.getString("category_id"));
			  catbean.setCategory_name(rs.getString("subject_category"));
			  sb.setScb(catbean);
			  Sub_SubjectBean ss = new  Sub_SubjectBean();
			  ss.setSub_subjectid(rs.getInt("sub_subid"));
			  ss.setSub_subject_name(rs.getString("sub_subname"));
			  sb.setSub_subbean(ss);
			}
		}catch(SQLException  e){
			System.out.println(" show error! ===> Sub_SubJectService ===== > :gatAsubjectCategory()" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return sb;
	}
	
		         
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////
	public static ArrayList<Sub_SubjectBean> getAllSub_subject(){
		
		ArrayList<Sub_SubjectBean> sb = new ArrayList<Sub_SubjectBean>();
		
		
		try{
			String sql = "SELECT ss.*,s.* FROM t_sub_subject ss INNER JOIN t_subject s ON ss.subject_id = s.subject_id ";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Sub_SubjectBean sub_sub = new Sub_SubjectBean();
				sub_sub.setSub_subjectid(rs.getInt("sub_subid"));
				SubjectBean sbean = new SubjectBean();
				sbean.setSubject_name(rs.getString("subject_name"));
				 sub_sub.setSubject(sbean);  
				sub_sub.setSub_subject_name(rs.getString("sub_subname"));
				
				sb.add(sub_sub);
			
			}
		}catch(SQLException  e){
			System.out.println(" show error! ===> Sub_SubJectService ===== > :gatAllSub_subject()" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return sb;
	}
	

	
	
	
	
}
