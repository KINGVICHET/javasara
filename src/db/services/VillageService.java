package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import bean.CommuneBean;
import bean.VillageBean;
import db.mysql.MySQL;

public class VillageService {
	
	public static String getNewId(){
		String code = null;
		
        try{
        	String sql = "SELECT MAX(village_id) AS villageID FROM t_village";
        	PreparedStatement ps =MySQL.P_getConnection().prepareStatement(sql);
        	ResultSet rs = ps.executeQuery();
        	if(rs.next()){
        		String cCode = rs.getString("villageID");
        		String l_code = l_cutePrefix(cCode, "vil");
        		int tmpCode = Integer.parseInt(l_code);
        		tmpCode ++;
        		
        		code = "vil";
        		code += String.format("%03d", tmpCode);
        	}
        	
        }catch(Exception e){
        	code = "vil";
        	code += String.format("%03d", 1);
        	System.out.println("VillageService::getNewId() => " + e.toString());
        }finally{
        	MySQL.P_getClose();
        }
		
		return code;
	}
	
	//=========================================================================
		private static String l_cutePrefix(String str, String pref){
			if(str != null && pref != null && str.startsWith(pref)){
				return str.substring(pref.length());
			}
			return str;
		}
	
	public static ArrayList<VillageBean> p_listAllVillageByCommuneID(String id){
		ArrayList<VillageBean> al = new ArrayList<VillageBean>();
		try{
			String sql = "SELECT * FROM t_village WHERE commune_id = ?";
			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				VillageBean vb = new VillageBean();
				vb.setVillage_id(rs.getString("village_id"));
				vb.setVillage(rs.getString("village"));
				vb.setCommune_id(rs.getString("commune_id"));
				al.add(vb);
			}
		}catch(Exception e){
			System.out.println(" show error!" + e.toString());
		}finally{
			MySQL.P_getClose();
		}
		return al;
	}

	public static String addNewVillage(VillageBean vb){
		String message = "";
		try{
        	String sql = "INSERT INTO t_village VALUES(?,?,?)";
        	PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
        	ps.setString(1, vb.getVillage_id());
        	ps.setString(2, vb.getVillage());
        	
        	CommuneBean cb = vb.getCb();
        	ps.setString(3, cb.getCommune_id());
			
        	int state = ps.executeUpdate();
        	if(state > 0 ){		
				message = "success";
			}
		}catch(Exception e){
			message = "error";
        	System.out.println("VillageService::addNewVillage() => " + e.toString());
        }finally{
        	MySQL.P_getClose();
        }
		return message;
	}
}
