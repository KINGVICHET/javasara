package db.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import bean.ClassStudyBean;
import bean.RoomBean;
import bean.TeacherBean;
import bean.TeacherClassBean;
import db.mysql.MySQL;

public class TeacherClassService {
	//=================================== Insert Multi Row of TeacherClass ============================
	   public static String addTeacherClass(ArrayList<TeacherClassBean> list){
		   String msg ="";
		   try{			   
		   		for(int i=0;i<list.size();i++){
		   			String sql = "INSERT INTO t_teacher_class VALUES(?,?)";
		   			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
		   			TeacherClassBean tcb = (TeacherClassBean) list.get(i);
		   			ps.setString(1, tcb.getClass_id());
		   			ps.setString(2, tcb.getTeacher_id());
		   			
		   			int state = ps.executeUpdate();
		   			if(state > 0){
		   				msg = "Success";
		   			}
		   		}
		   }catch(Exception e){
			   msg = "Error";
			   System.out.println("TeacherClassService::addTeacherClass() => " + e.toString());
		   }finally{
			   MySQL.P_getClose();
		   }
		   return msg;
	   }
	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////// GET CLASS TO TEACHER ID//////
	   public static TeacherBean DisplayTeacherClassToteacherId(String teacher_id){
		   TeacherBean ut= null;
		   try{			   
		   	
		   			String sql = "SELECT t.*, tcs.* FROM t_teacher t INNER JOIN (t_teacher_class tc INNER JOIN  "
		   					+ "t_class_study tcs on tc.class_id = tcs.class_id on tc.teacher_id = t.teacher_id "
		   					+ "where tcs.class_id = tcs.class_id";
		   			PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
		   			     ps.setString(1, teacher_id);
					     ResultSet rs = ps.executeQuery();
					
					while(rs.next())
						{
						   ut= new TeacherBean();
						   ut.setT_id(rs.getString("teacher_id"));
						   ut.setT_fname(rs.getString("first_name"));
						   ut.setT_lname(rs.getString("last_name"));
						   ClassStudyBean cl = new ClassStudyBean(); 
						    cl.setClass_id(rs.getString("class_id"));
						    cl.setClass_name(rs.getString("class_name"));
						    cl.setTime_study("time_study");
						    ut.setClassstudybean(cl);
						  
						}
		   		
		   }catch(Exception e){
			  
			   System.out.println("TeacherClassService::DisplayTeacherClassToteacherId() => " + e.toString());
		   }finally{
			   MySQL.P_getClose();
		   }
		   return ut;
	   }
	   
	   //================= list Teachers by Class id ================
	   public static ArrayList<TeacherBean> getTeacherByClassID(String id){
			ArrayList<TeacherBean> al = new ArrayList<TeacherBean>();
			try{
				String sql = "SELECT t.*,tc.* FROM t_teacher t INNER JOIN t_teacher_class tc ON t.teacher_id = tc.teacher_id WHERE tc.class_id = ?";
				PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					TeacherBean tb = new TeacherBean();
					tb.setT_id(rs.getString("teacher_id"));
					tb.setT_fname(rs.getString("first_name"));
					tb.setT_lname(rs.getString("last_name"));
					tb.setT_gender(rs.getString("gender"));
					tb.setT_phone(rs.getString("phone"));
					tb.setT_photo(rs.getString("photo"));
					tb.setPhoto_url(rs.getString("photo_url"));
					
					al.add(tb);
				}
			}catch(Exception e){
				System.out.println("TeacherClassService::getTeacherByClassID() => " + e.toString());System.out.println(" show error!" + e.toString());
			}finally{
				MySQL.P_getClose();
			}
			return al;
		}
	   
	 //================= list Class by Teacher id ================
	   public static ArrayList<TeacherClassBean> getClassByTeacherID(String id){
			ArrayList<TeacherClassBean> al = new ArrayList<TeacherClassBean>();
			try{
				String sql = "SELECT t.*,tc.*, cs.* FROM t_teacher t INNER JOIN (t_teacher_class tc INNER JOIN t_class_study cs ON tc.class_id = cs.class_id)ON t.teacher_id = tc.teacher_id  WHERE tc.teacher_id =?";
				PreparedStatement ps = MySQL.P_getConnection().prepareStatement(sql);
				ps.setString(1, id);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					TeacherClassBean tcb = new TeacherClassBean();
					ClassStudyBean csb = new ClassStudyBean();
					csb.setClass_id(rs.getString("class_id"));
					csb.setClass_name(rs.getString("class_name"));
					//RoomBean rb = new RoomBean();
					//rb.setRoom_id(rs.getString("room_id"));
					//rb.setRoom(rs.getString("room_name"));
					//tcb.setRb(rb);
					
					System.out.print("Room Name = " + tcb.getRb().getRoom());
					
					tcb.setTeacher_id(rs.getString("teacher_id"));
					
					al.add(tcb);
				}
			}catch(Exception e){
				System.out.println("TeacherClassService::getTeacherByClassID() => " + e.toString());System.out.println(" show error!" + e.toString());
			}finally{
				MySQL.P_getClose();
			}
			return al;
		}
}
