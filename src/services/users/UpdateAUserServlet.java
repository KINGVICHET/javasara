package services.users;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import bean.RoleBean;
import bean.UserBean;

@WebServlet("/UpdateAUserServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
						maxFileSize = 1024 * 1024 * 50,
						maxRequestSize = 1024 * 1024 * 100
)
public class UpdateAUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public UpdateAUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	    response.setContentType("text/html; charset=UTF-8");//display HTML from servlet.
		
		String uid = request.getParameter("user_id");
		String uname = request.getParameter("user_name");
		String pass = request.getParameter("password");
		String hin = request.getParameter("hin");
		String question = request.getParameter("question");
		String ans = request.getParameter("answer");
		String role = request.getParameter("role");
		
		Part part = request.getPart("file");
		String fileName = fileNameFilter(part);
		
			UserBean ub = new UserBean();
			RoleBean rb = new RoleBean();
		if(!fileName.equals("")){ // have new photo to update
			String savePath = "\\images\\users\\";
			Path path = Paths.get("images/users");
			String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\users\\" + File.separator + fileName;
			System.out.println("url = " + urlPath);
			File fileSaveDirectory = new File(urlPath);
			part.write(urlPath + File.separator);
			
			//to deletេ old photo
			ServletContext context = getServletContext();
			String oldFileName = request.getParameter("old_photo");
			deleteExistImage(context,oldFileName);
			
			ub.setUser_id(uid);
			ub.setUser_name(uname);
			ub.setPassword(pass);
			ub.setHin(hin);
			ub.setQuestion(question);
			ub.setAnswer(ans);
			rb.setRole_name(role);
			ub.setRb(rb);
			ub.setPhoto_name(fileName);
			ub.setPhoto_url(savePath);
			
		}else{
			ub.setUser_id(uid);
			ub.setUser_name(uname);
			ub.setPassword(pass);
			ub.setHin(hin);
			ub.setQuestion(question);
			ub.setAnswer(ans);
			rb.setRole_name(role);
			ub.setRb(rb);
			ub.setPhoto_name("");
		}
		String msg = db.services.UserService.p_updateExistUserAccount(ub); // SUC ឬ   ERR
		HttpSession session = request.getSession(true);
		session.setAttribute("code", msg);
		response.sendRedirect("./view/detail/EditAUser?id=" + ub.getUser_id());
}

	private void deleteExistImage(ServletContext context, String imageName){
		String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\users\\" + File.separator + imageName;
		new File(urlPath).delete();
		
	}
	
	public static String fileNameFilter(Part part){
		String contentDisplay = part.getHeader("content-disposition");
		
		System.out.println("contentDisplay = " + contentDisplay);
		String []items = contentDisplay.split(";");
		for(String str : items){
			if(str.trim().startsWith("filename")){
				String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
				System.out.println("fname = " + name);
				String []fname = name.split( Pattern.quote(File.separator) );
				return fname[fname.length - 1];
				//return str.substring(str.indexOf("=") + 2, str.length()-1);
			}
		}
		return "";
	}

}
