package services.subject_category;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.SubjectCategoryBean;
import db.services.SubjectCategoryService;

@WebServlet({ "/DeleteCategory_subject", "/Delete_category_subject" })
public class DeleteCategory_subject extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public DeleteCategory_subject() {
        super();
    }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String id = request.getParameter("id");
		
		
		
		SubjectCategoryBean scb = new SubjectCategoryBean();
		scb.setCategory_id(id);
		
		String msg = SubjectCategoryService.P_Delete_Subject_Category(id);
		HttpSession session = request.getSession(true);
		session.setAttribute("code", msg);
		response.sendRedirect(".?id="+scb.getCategory_id());
	}

}
