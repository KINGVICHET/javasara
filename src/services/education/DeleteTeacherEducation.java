package services.education;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import bean.EducationBean;
import bean.TeacherBean;
import db.services.EducationService;

@WebServlet({ "/DeleteTeacherEducation", "/DeleteTeacherEductionById" })
public class DeleteTeacherEducation extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public DeleteTeacherEducation() {
        super();
        
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String id = request.getParameter("id");
		 
	    System.out.print("Educatn id = " + id);
	    String result = db.services.EducationService.DeleteEducationTeacher(id);
	    String msg = new Gson().toJson(result);
	    
	   
	    response.setContentType("application/json");
	    request.setCharacterEncoding("UTF-8");
	    response.getWriter().write(msg);
	}
}
