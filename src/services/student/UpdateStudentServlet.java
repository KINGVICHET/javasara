package services.student;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import bean.AddressBean;
import bean.StudentBean;

@WebServlet("/UpdateStudentServlet")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10,
						maxFileSize = 1024 * 1024 * 50,
						maxRequestSize = 1024 * 1024 * 100
)
public class UpdateStudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UpdateStudentServlet() {super(); }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	    response.setContentType("text/html; charset=UTF-8");//display HTML from servlet.
	    
		String stu_id = request.getParameter("student_id");
		String fname = request.getParameter("first_name");
		String lname = request.getParameter("last_name");
		String gender =request.getParameter("gender");
		String nation = request.getParameter("nationality");
		String dob = request.getParameter("dob");
		String pob_address = request.getParameter("pob_village_id");
		String current_address = request.getParameter("current_village_id");
		String phone = request.getParameter("phone_number");
		String guardian = request.getParameter("guardian_id");
		
		Part part = request.getPart("file");
		String fileName = fileNameFilter(part);
		System.out.println("fileName = " + fileName);
		
		StudentBean sb = new StudentBean();
		AddressBean pob = new AddressBean();
		AddressBean current_add = new AddressBean();
		
		if(!fileName.equals("")){ // have new photo to update
			String savePath = "\\images\\students\\";
			Path path = Paths.get("images/students");
			String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\students\\" + File.separator + fileName;
			System.out.println("url = " + urlPath);
			File fileSaveDirectory = new File(urlPath);
			part.write(urlPath + File.separator);
			
			//to deletេ old photo
			ServletContext context = getServletContext();
			String oldFileName = request.getParameter("old_photo");
			deleteExistImage(context,oldFileName);
			
			sb.setStudent_id(stu_id);
			sb.setFirst_name(fname);
			sb.setLast_name(lname);
			sb.setGender(gender);
			sb.setNationality(nation);
			sb.setDob(dob);
			
			pob.setVillage_id(pob_address);
			sb.setAb(pob);
			
			current_add.setVillage_id(current_address);
			sb.setAbCurrent(current_add);
			
			sb.setPhone(phone);
			sb.setGuardian_id(guardian);
			sb.setPhoto_name(fileName);
			sb.setPhoto_url(savePath);
		}else{
			sb.setStudent_id(stu_id);
			sb.setFirst_name(fname);
			sb.setLast_name(lname);
			sb.setGender(gender);
			sb.setNationality(nation);
			sb.setDob(dob);
			
			pob.setVillage_id(pob_address);
			sb.setAb(pob);
			
			current_add.setVillage_id(current_address);
			sb.setAbCurrent(current_add);
			
			sb.setPhone(phone);
			sb.setGuardian_id(guardian);
			sb.setPhoto_name("");
		}
		
		String msg = db.services.StudentService.updateAStudent(sb);
		HttpSession session = request.getSession(false);
		session.setAttribute("code", msg);
		response.sendRedirect("./view/update/EditStudent?id=" + sb.getStudent_id());
		
	}
	
	private void deleteExistImage(ServletContext context, String OldImageName){
		String urlPath = "D:\\JavaProjects\\LMS\\WebContent\\images\\students\\" + File.separator + OldImageName;
		new File(urlPath).delete();
		
	}
	
	public static String fileNameFilter(Part part){
		String contentDisplay = part.getHeader("content-disposition");
		
		System.out.println("contentDisplay = " + contentDisplay);
		String []items = contentDisplay.split(";");
		for(String str : items){
			if(str.trim().startsWith("filename")){
				String name = str.substring(str.indexOf("=") + 2, str.length() -1 );
				System.out.println("fname = " + name);
				String []fname = name.split( Pattern.quote(File.separator) );
				return fname[fname.length - 1];
				//return str.substring(str.indexOf("=") + 2, str.length()-1);
			}
		}
		return "";
	}

}
