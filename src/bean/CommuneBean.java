package bean;

public class CommuneBean {
	private String commune_id,commune,district_id;
	private DistrictBean db;
	
	public DistrictBean getDb() {
		return db;
	}
	public void setDb(DistrictBean db) {
		this.db = db;
	}
	public CommuneBean(){}
	public CommuneBean(String id,String com,String distid){
		this.commune_id = id;
		this.commune = com;
		this.district_id = distid;
	}
	public String getCommune_id() {
		return commune_id;
	}
	public void setCommune_id(String commune_id) {
		this.commune_id = commune_id;
	}
	public String getCommune() {
		return commune;
	}
	public void setCommune(String commune) {
		this.commune = commune;
	}
	public String getDistrict_id() {
		return district_id;
	}
	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}
	
}
