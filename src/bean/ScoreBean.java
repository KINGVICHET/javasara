package bean;

public class ScoreBean {
	private String exam_start_date, exam_end_date,SubjectCategory_id,SubjectCategory_name;
	private SubjectCategoryBean category ; private Sub_SubjectBean sub_subject;
	
	public Sub_SubjectBean getSub_subject() {
		return sub_subject;
	}
	public void setSub_subject(Sub_SubjectBean sub_subject) {
		this.sub_subject = sub_subject;
	}
	public String getExam_start_date() {
		return exam_start_date;
	}
	public void setExam_start_date(String exam_start_date) {
		this.exam_start_date = exam_start_date;
	}
	public String getExam_end_date() {
		return exam_end_date;
	}
	public void setExam_end_date(String exam_end_date) {
		this.exam_end_date = exam_end_date;
	}
	public String getSubjectCategory_id() {
		return SubjectCategory_id;
	}
	public void setSubjectCategory_id(String subjectCategory_id) {
		SubjectCategory_id = subjectCategory_id;
	}
	public String getSubjectCategory_name() {
		return SubjectCategory_name;
	}
	public void setSubjectCategory_name(String subjectCategory_name) {
		SubjectCategory_name = subjectCategory_name;
	}
	public SubjectCategoryBean getCategory() {
		return category;
	}
	public void setCategory(SubjectCategoryBean category) {
		this.category = category;
	}
	
	public ScoreBean(String exam_start_date, String exam_end_date, String subjectCategory_id,
			String subjectCategory_name, SubjectCategoryBean category) {
		super();
		this.exam_start_date = exam_start_date;
		this.exam_end_date = exam_end_date;
		SubjectCategory_id = subjectCategory_id;
		SubjectCategory_name = subjectCategory_name;
		this.category = category;
	}
    
}
