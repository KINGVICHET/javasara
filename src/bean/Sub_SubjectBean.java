package bean;

public class Sub_SubjectBean {
	private String sub_subject_name;
	private int sub_subjectid;
	public int getSub_subjectid() {
		return sub_subjectid;
	}
	public void setSub_subjectid(int sub_subjectid) {
		this.sub_subjectid = sub_subjectid;
	}
	private SubjectBean subject;
	private SubjectCategoryBean subject_category;
	public SubjectCategoryBean getSubject_category() {
		return subject_category;
	}
	public void setSubject_category(SubjectCategoryBean subject_category) {
		this.subject_category = subject_category;
	}
	
	public String getSub_subject_name() {
		return sub_subject_name;
	}
	public void setSub_subject_name(String sub_subject_name) {
		this.sub_subject_name = sub_subject_name;
	}
	public SubjectBean getSubject() {
		return subject;
	}
	public void setSubject(SubjectBean subject) {
		this.subject = subject;
	}

}
