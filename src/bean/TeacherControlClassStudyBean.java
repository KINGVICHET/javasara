package bean;

public class TeacherControlClassStudyBean {

	public TeacherControlClassStudyBean() {
		
		
	}
	private TeacherBean teacherbean;
	private ClassStudyBean classstudybean;
	public TeacherBean getTeacherbean() {
		return teacherbean;
	}
	public void setTeacherbean(TeacherBean teacherbean) {
		this.teacherbean = teacherbean;
	}
	public ClassStudyBean getClassstudybean() {
		return classstudybean;
	}
	public void setClassstudybean(ClassStudyBean classstudybean) {
		this.classstudybean = classstudybean;
	}

}
